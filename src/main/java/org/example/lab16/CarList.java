package org.example.lab16;

class Car {
    int year;
    double engine;
    String model;
    Car next;

    Car() {
        year = 2022;
        engine = 2.0;
        model = "Kia";
    }

    Car(int y, double e, String m) {
        year = y;
        engine = e;
        model = m;
    }
}

public class CarList {
    int total;
    Car head;
    Car back;

    CarList() {
        head = null;
        total = 0;
    }

    public void SortYear() {
        Car newcar = new Car();
        back = head;

        // sortoo hiih code-g bichne uu
    }

    public void addFront(Car c) {
        Car newcar = new Car();
        newcar = c;
        newcar.next = head;
        head = newcar;
        total++;
    }

    public void addBack(Car c) {
        back = head;
        while (back.next != null) {
            back = back.next;
        }
        Car newcar = new Car();
        newcar = c;
        back.next = newcar;
        total++;
    }

    public void addMid(Car c, int p) {
        Car newcar = new Car();
        newcar = c;
        int i = 0;
        back = head;
        while (i < p - 1) {
            back = back.next;
            i++;
        }
        newcar.next = back.next;
        back.next = newcar;
        total++;
    }


    public void show() {
        back = head;
        int count = 1;
        while (back != null) {
            System.out.print("\n" + count + " Car number: " + back.engine + ", " + back.year);
//            System.out.print("\n\nYear: " + back.year + "\nengine: " + back.engine + "\nmodel: " + back.model);
            back = back.next;
            count++;
        }
    }

    public void showNumberOfCars() {
        System.out.println("\n\nNumber of cars: " + total);
    }

    public void delLast() {
        back = head;
        while (back.next.next != null) {
            back = back.next;
        }
        back.next = null;
        total--;
    }

    public void delFirst() {
        head = head.next;
        total--;
    }

    public void delMiddle(int position) {
        back = head;
        int i = 0;
        while (i < position - 1) {
            back = back.next;
            i++;
        }
        back.next = back.next.next;
        total--;
    }

    public void sortAsc() {
        int j = 1;
        while (j < total) {
            back = head;
            int i = 0;
            while (i < total - j) {
                if (back.next != null && back.year > back.next.year) {
                    Car buffer = new Car(back.next.year, back.next.engine, back.next.model);
                    back.next = back.next.next;
                    buffer.next = head;
                    head = buffer;
                } else {
                    while (back.next != null && back.next.next != null) {
                        if (back.next.year > back.next.next.year) {
                            Car buffer = new Car(back.next.year, back.next.engine, back.next.model);
                            back.next = back.next.next;
                            buffer.next = back.next.next;
                            back.next.next = buffer;
                        }
                        back = back.next;
                    }
                }
                i++;
            }
            j++;
        }
    }

    public static void main(String[] args) {

        CarList carList = new CarList();

        Car c1 = new Car(1999, 1, "Hyundai");
        Car c2 = new Car(2001, 2, "Toyota");
        Car c3 = new Car(1995, 3, "Mazda");
        Car c4 = new Car(2000, 4, "Ford");
        Car c5 = new Car(1955, 5, "Pon");
        Car c6 = new Car(1987, 6, "test");
        Car c7 = new Car(2011, 7, "test-1");
        Car c8 = new Car(1998, 8, "test-2");
        Car c9 = new Car(2004, 9, "test-3");
        Car c10 = new Car(1960, 10, "test-4");
        Car c11 = new Car(1972, 11, "test-5");
        Car c12 = new Car(2015, 12, "test-6");
        Car c13 = new Car(1985, 13, "test-7");
        Car c14 = new Car(2023, 14, "test-8");
        Car c15 = new Car(1965, 15, "test-9");
        Car c16 = new Car(1990, 16, "test-10");
        Car c17 = new Car(2005, 17, "test-11");
        Car c18 = new Car(2018, 18, "test-12");
        Car c19 = new Car(2022, 19, "test-13");
        Car c20 = new Car(1978, 20, "test-14");


        carList.addFront(c1);
        carList.addBack(c2);
        carList.addBack(c3);
        carList.addBack(c4);
        carList.addBack(c5);
        carList.addBack(c6);
        carList.addBack(c7);
        carList.addBack(c8);
        carList.addBack(c9);
        carList.addBack(c10);
        carList.addBack(c11);
        carList.addBack(c12);
        carList.addBack(c13);
        carList.addBack(c14);
        carList.addBack(c15);
        carList.addBack(c16);
        carList.addBack(c17);
        carList.addBack(c18);
        carList.addBack(c19);
        carList.addBack(c20);

        carList.sortAsc();
        carList.show();
        carList.showNumberOfCars();
    }
}

