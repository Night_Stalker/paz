package org.example.lab11;

import java.util.Scanner;

public class Lab11 {

    private static boolean checksSpecialCharacters(String word) {
        String specialCharacters = "!@#$%^&*()_+-=[]{};':\",.<>/?";
        for (char charWord : specialCharacters.toCharArray()) {
            if (word.contains(String.valueOf(charWord))) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {

        String id = "Qwerty013";
        String password = "Ub#99042310";
        Scanner scanner = new Scanner(System.in);
        boolean isMatches = false;

        for (int i = 0; i < 5; i++) {
            System.out.print("ID oruul:");
            String userId = scanner.nextLine();
            if (Character.isLetter(userId.charAt(0)) && userId.length() >= 6 && userId.length() <= 12) {
                System.out.print("Password oruul:");
                String userPassword = scanner.nextLine();
                if (userPassword.matches(".*[a-z].*") &&
                        userPassword.matches(".*[A-Z].*") &&
                        userPassword.matches(".*\\d.*") &&
                        checksSpecialCharacters(userPassword)) {
                    System.out.println("Id bolon password shalguur hangaj bn.");
                    if (userId.equals(id) && userPassword.equals(password)) {
                        System.out.println("Id password taarlaa.");
                        isMatches = true;
                        break;
                    } else {
                        System.out.println("Ner esvel password zurj bn.");
                        System.out.println("Tanid " + (4 - i) + " oroldlogo uldee");
                    }
                } else {
                    System.out.println("Password 1 tom useg, 1 jijig useg, 1 too, 1 tusgai temdegt aguulsan bh ystoi.");
                    System.out.println("Tanid " + (4 - i) + " oroldlogo uldee");
                }
            } else {
                System.out.println("Id 6 aas 12-iin urttai usgeer ehelsen bh ystoi.");
                System.out.println("Tanid " + (4 - i) + " oroldlogo uldee");
            }
        }
        scanner.close();
        if (!isMatches) {
            System.out.println("LOCKED");
        } else {
            System.out.println("BINGO");
        }
    }
}