package org.example;

import javakara.JavaKaraProgram;

public class DrawTriangles extends JavaKaraProgram {

    private void putLeaf() {
        if (!kara.onLeaf()) {
            kara.putLeaf();
        }
    }

    private void configuration(int yAxis) {
        world.clearAll();
        world.setSize(23, 23);
        kara.setPosition(11, yAxis);
    }

    private void startLeaf() {
        putLeaf();
        kara.move();
        kara.turnRight();
        kara.move();
        kara.turnRight();
        putLeaf();
    }

    private boolean checkTopLeaf(boolean toLeft) {
        boolean haveLeaf;
        kara.move();
        if (toLeft) {
            kara.turnRight();
            kara.move();
            haveLeaf = kara.onLeaf();
            kara.turnRight();
            kara.turnRight();
            kara.move();
            kara.turnRight();
        } else {
            kara.turnLeft();
            kara.move();
            haveLeaf = kara.onLeaf();
            kara.turnLeft();
            kara.turnLeft();
            kara.move();
            kara.turnLeft();
        }
        return haveLeaf;
    }

    private boolean checkBreakStatement(int yAxis) {
        int vert = kara.getPosition().y;
        return vert == yAxis + 12;
    }

    public void myProgram() {
        int yAxis = 5;
        configuration(yAxis);
        startLeaf();
        boolean toLeft = true;
        while (true) {
            if (checkTopLeaf(toLeft)) {
                putLeaf();
            } else {
                if (toLeft) {
                    putLeaf();
                    kara.move();
                    kara.turnLeft();
                    kara.move();
                    kara.turnLeft();
                    if (checkBreakStatement(yAxis)) {
                        break;
                    }
                    putLeaf();
                    toLeft = false;
                } else {
                    putLeaf();
                    kara.move();
                    kara.turnRight();
                    kara.move();
                    kara.turnRight();
                    if (checkBreakStatement(yAxis)) {
                        break;
                    }
                    putLeaf();
                    toLeft = true;
                }
            }
        }
    }
}
