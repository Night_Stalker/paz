package org.example;

import javakara.JavaKaraProgram;

public class Slalom extends JavaKaraProgram {

    private boolean bothSideHaveTree() {
        return kara.treeLeft() && kara.treeRight();
    }

    private void left() {
        kara.move();
        kara.turnLeft();
        kara.move();
        kara.move();
        kara.turnLeft();
        kara.move();
    }

    private void right() {
        kara.move();
        kara.turnRight();
        kara.move();
        kara.move();
        kara.turnRight();
        kara.move();
    }

    public void myProgram() {
        boolean downOrLeft = true;
        boolean goToLeft = true;
        while (true) {
            if (goToLeft) {
                if (downOrLeft && kara.treeLeft() && !kara.treeRight()) {
                    left();
                    if (!bothSideHaveTree()) {
                        goToLeft = false;
                    }
                    downOrLeft = false;
                } else if (downOrLeft && bothSideHaveTree()) {
                    left();
                    downOrLeft = false;
                } else if (!downOrLeft && bothSideHaveTree()) {
                    right();
                    downOrLeft = true;
                } else {
                    right();
                    downOrLeft = false;
                    goToLeft = false;
                }
            } else {
                if (downOrLeft && kara.treeLeft() && !kara.treeRight()) {
                    left();
                    downOrLeft = false;
                    goToLeft = true;
                } else if (downOrLeft && bothSideHaveTree()) {
                    right();
                    downOrLeft = false;
                } else if (!downOrLeft && bothSideHaveTree()) {
                    left();
                    downOrLeft = true;
                } else if (!downOrLeft && kara.treeLeft() && !kara.treeRight()) {
                    left();
                    downOrLeft = true;
                    if (!bothSideHaveTree()) {
                        goToLeft = true;
                    }
                } else {
                    right();
                    downOrLeft = false;
                }
            }
        }
    }
}

