package org.example.lab12;

import java.util.Arrays;
import java.util.Comparator;

public class Lab12 {

    static String[] gender = {"Male", "FeMale"};

    private static float avgAge(Person[] persons) {
        float totalAge = 0;
        for (Person person : persons) {
            totalAge += person.getAge();
        }
        return totalAge / persons.length;
    }

    private static Person findOldest(Person[] persons) {
        Person oldestPerson = persons[0];
        for (Person person : persons) {
            if (person.getAge() > oldestPerson.getAge()) {
                oldestPerson = person;
            }
        }
        return oldestPerson;
    }

    private static Person findYoungest(Person[] persons) {
        Person youngestPerson = persons[0];
        for (Person person : persons) {
            if (person.getAge() < youngestPerson.getAge()) {
                youngestPerson = person;
            }
        }
        return youngestPerson;
    }

    public static void main(String[] args) {
        Person[] persons = new Person[5];

//        garaas medeelel oruulah heseg
//        String name;
//        int age;
//        String gender;
//
//        for (int i = 0; i < persons.length; i++) {
//            name = JOptionPane.showInputDialog(null, "Neriig oruul:");
//            age = Integer.parseInt(JOptionPane.showInputDialog(null, "Nas oruul:"));
//            gender = JOptionPane.showInputDialog(null, "Huisiig oruul:");
//
//            persons[i] = new Person(name, age, gender);
//        }

//         Defualt
        persons[0] = new Person("Alice", 20, gender[1]);
        persons[1] = new Person("Bob", 22, gender[0]);
        persons[2] = new Person("Charlie", 23, gender[0]);
        persons[3] = new Person("David", 18, gender[0]);
        persons[4] = new Person("Eve", 34, gender[1]);

        System.out.println("Avarage age is:" + avgAge(persons));

        System.out.println("\nOldest person is:");
        findOldest(persons).printPerson();

        System.out.println("\nYoungest person is:");
        findYoungest(persons).printPerson();

        Arrays.sort(persons, Comparator.comparingInt(Person::getAge));
        System.out.println("\nSorted persons:");
        for (Person person : persons) {
            person.printPerson();
        }
    }
}