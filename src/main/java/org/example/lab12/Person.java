package org.example.lab12;

class Person {

    private String name;
    private int age;
    private String gender;

    public Person() {
        this("Not Given", 0, "U");
    }

    public Person(String name, int age, String gender) {
        this.age = age;
        this.name = name;
        this.gender = gender;
    }

    public void printPerson() {
        System.out.println("Name is:" + this.name);
        System.out.println("Age is:" + this.age);
        System.out.println("Gender is:" + this.gender);
    }


    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }


    public String getName() {
        return name;
    }


    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}