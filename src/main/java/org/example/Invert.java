package org.example;

import javakara.JavaKaraProgram;

public class Invert extends JavaKaraProgram {

    private boolean checkLeftTree() {
        return kara.treeFront() && kara.treeLeft();
    }

    private boolean checkRightTree() {
        return kara.treeFront() && kara.treeRight();
    }

    private void invertLeaf() {
        if (kara.onLeaf()) {
            kara.removeLeaf();
        } else {
            kara.putLeaf();
        }
    }

    public void myProgram() {
        boolean toLeft = true;
        while ((toLeft && !checkRightTree()) || (!toLeft && !checkLeftTree())) {
            if (!kara.treeFront()) {
                invertLeaf();
                kara.move();
            } else {
                invertLeaf();
                if (toLeft) {
                    kara.turnRight();
                    kara.move();
                    kara.turnRight();
                    toLeft = false;
                } else {
                    kara.turnLeft();
                    kara.move();
                    kara.turnLeft();
                    toLeft = true;
                }
            }
        }
        invertLeaf();
    }
}
