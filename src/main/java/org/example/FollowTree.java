package org.example;

import javakara.JavaKaraProgram;

public class FollowTree extends JavaKaraProgram {

    public void myProgram() {
        kara.move();
        while (true) {
            if (!kara.treeFront() && kara.treeRight()) {
                kara.move();
            } else if (!kara.treeRight()) {
                kara.turnRight();
                kara.move();
            } else if (kara.treeFront() && kara.treeRight() && kara.treeLeft()) {
                kara.turnRight();
                kara.turnRight();
                kara.move();
                if (!kara.treeRight()) {
                    kara.turnRight();
                    kara.move();
                }
            } else if (kara.treeFront() && !kara.treeLeft()) {
                kara.turnLeft();
                kara.move();
            }
        }
    }
}