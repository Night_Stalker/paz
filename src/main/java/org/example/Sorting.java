package org.example;

import javakara.JavaKaraProgram;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Sorting extends JavaKaraProgram {

    private boolean checkLeaf() {
        return kara.onLeaf();
    }

    private Integer countRow() {
        int count = 0;
        while (true) {
            if (checkLeaf()) {
                count++;
            }
            if (kara.getPosition().getX() == world.getSizeX() - 1) {
                break;
            }
            kara.move();
        }
        return count;
    }

    public void myProgram() {
        kara.setPosition(0, 0);
        List<Integer> values = new ArrayList<>();
        while (true) {
            int numberOfLeaf = countRow();
//            tools.showMessage(String.valueOf(numberOfLeaf));
            values.add(numberOfLeaf);
            if ((int) kara.getPosition().getX() == world.getSizeX() - 1
                    && (int) kara.getPosition().getY() == world.getSizeY() - 1) {
                break;
            }
            kara.move();
            kara.turnRight();
            kara.move();
            kara.turnLeft();
        }
        world.clearAll();
        kara.setPosition(0, 0);
        Collections.sort(values);
        for (Integer value : values) {
//            tools.showMessage(String.valueOf(value));
            for (int i = 0; i < value; i++) {
                if (!kara.onLeaf()) {
                    kara.putLeaf();
                }
                kara.move();
            }
            if ((int) kara.getPosition().getY() + 1 < world.getSizeY()) {
                kara.setPosition(0, (int) kara.getPosition().getY() + 1);
            } else {
                break;
            }
        }
    }
}
