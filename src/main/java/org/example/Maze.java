package org.example;

import javakara.JavaKaraProgram;

public class Maze extends JavaKaraProgram {

    private boolean goToLeft;

    private void moveForward() {
        if (!kara.treeFront()) {
            kara.move();
        }
    }

    private void goToBack() {
        kara.turnLeft();
        kara.turnLeft();
    }

    private void goToLeft() {
        if (!kara.treeLeft()) {
            kara.turnLeft();
            moveForward();
            moveForward();
            kara.turnRight();
        } else if (kara.treeFront()) {
            goToBack();
            this.goToLeft = false;
        } else {
            moveForward();
        }
    }

    private void goToRight() {
        if (!kara.treeRight()) {
            kara.turnRight();
            moveForward();
            moveForward();
            kara.turnLeft();
        } else if (kara.treeFront()) {
            goToBack();
            this.goToLeft = true;
        } else {
            moveForward();
        }
    }

    public void myProgram() {
        this.goToLeft = true;
        while (!kara.onLeaf()) {
            if (goToLeft) {
                goToLeft();
            } else {
                goToRight();
            }
        }
        kara.removeLeaf();
    }
}
