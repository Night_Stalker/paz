package org.example;

import javakara.JavaKaraProgram;

/*
 * COMMANDS:
 *   kara.move()           kara.turnRight()      kara.turnLeft()
 *   kara.putLeaf()        kara.removeLeaf()
 * SENSORS:
 *   kara.treeFront()      kara.treeLeft()       kara.treeRight()
 *   kara.mushroomFront()  kara.onLeaf()
 */
public class Lab4_5 extends JavaKaraProgram {
    //
    // you can define youimport javakara.JavaKaraProgram;
    //
    ///*
    // * COMMANDS:
    // *   kara.move()           kara.turnRight()      kara.turnLeft()
    // *   kara.putLeaf()        kara.removeLeaf()
    // * SENSORS:
    // *   kara.treeFront()      kara.treeLeft()       kara.treeRight()
    // *   kara.mushroomFront()  kara.onLeaf()
    // */
    //public class FindTree extends JavaKaraProgram {
    //  //
    //  // you can define your methods here:
    //  //
    //  public void myProgram() {
    //    // put your main program here, for example:
    //    while (!kara.treeFront()) {
    //      kara.move();
    //    }
    //  }
    //}
    //
    //        r methods here:
    //

    private void onTree() {
        while (kara.treeRight()) {
            kara.move();
        }
        kara.turnRight();
        kara.move();
        kara.turnLeft();
    }

    public void myProgram() {
        // put your main program here, for example:
        while (!kara.onLeaf()) {
            if (kara.treeFront()) {
                kara.turnLeft();
                kara.move();
                kara.turnRight();
                kara.move();
                onTree();
            } else {
                kara.move();
            }
        }
    }
}