package org.example;

import javakara.JavaKaraProgram;

public class DrawSpirals extends JavaKaraProgram {

    private void putLeaf() {
        if (!kara.onLeaf()) {
            kara.putLeaf();
        }
    }

    private void prepareToDraw() {
        putLeaf();
        kara.move();
        putLeaf();
        kara.turnRight();
        kara.move();
        putLeaf();
        kara.move();
        putLeaf();
        kara.turnRight();
    }

    private boolean checkInsideLeaf() {
        kara.move();
        kara.turnRight();
        kara.move();
        kara.move();
        boolean haveLeaf = kara.onLeaf();
        kara.turnRight();
        kara.turnRight();
        kara.move();
        kara.move();
        kara.turnRight();
        return haveLeaf;
    }

    public void myProgram() {
        prepareToDraw();
        int vert;
        while (true) {
            if (checkInsideLeaf()) {
                putLeaf();
            } else {
                vert = kara.getPosition().y;
                if (vert == 0) {
                    putLeaf();
                    break;
                }
                putLeaf();
                kara.move();
                putLeaf();
                kara.turnRight();
                kara.move();
                putLeaf();
            }
        }
    }
}
