package org.example.lab13;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;


public class Form extends JFrame {

    JLabel title, idLabel, nameLabel, genderLabel, addressLabel, contactLabel;
    JTextField idField, nameField, addressField, contactField;
    JButton registerButton, exitButton;
    JRadioButton male, female;
    ButtonGroup bg;
    JPanel panel;
    JTable table;
    DefaultTableModel model;
    JScrollPane scrollpane;

    Form() {
        setSize(700, 370);
        setLayout(null);

        title = new JLabel("Registration RegistrationForm");
        title.setBounds(60, 7, 200, 30);
        idLabel = new JLabel("ID");
        idLabel.setBounds(30, 50, 60, 30);
        nameLabel = new JLabel("Name");
        nameLabel.setBounds(30, 90, 60, 30);
        genderLabel = new JLabel("Gender");
        genderLabel.setBounds(30, 130, 60, 30);
        addressLabel = new JLabel("Address");
        addressLabel.setBounds(30, 170, 60, 30);
        contactLabel = new JLabel("Contact");
        contactLabel.setBounds(30, 210, 60, 30);

        idField = new JTextField();
        idField.setBounds(120, 50, 100, 30);
        nameField = new JTextField();
        nameField.setBounds(120, 90, 100, 30);
        addressField = new JTextField();
        addressField.setBounds(120, 170, 100, 30);
        contactField = new JTextField();
        contactField.setBounds(120, 210, 100, 30);

        male = new JRadioButton("Male");
        male.setBounds(95, 130, 60, 30);
        female = new JRadioButton("Female");
        female.setBounds(155, 130, 70, 30);
        bg = new ButtonGroup();
        bg.add(male);
        bg.add(female);

        exitButton = new JButton("Exit");
        exitButton.setBounds(30, 250, 85, 30);
        registerButton = new JButton("Register");
        registerButton.setBounds(120, 250, 100, 30);

        add(title);

        add(idLabel);
        add(nameLabel);
        add(genderLabel);
        add(addressLabel);
        add(contactLabel);

        add(idField);
        add(nameField);
        add(addressField);
        add(contactField);
        add(male);
        add(female);

        add(exitButton);
        add(registerButton);

        panel = new JPanel();
        panel.setLayout(new GridLayout());
        panel.setBounds(250, 10, 400, 300);
        panel.setBorder(BorderFactory.createDashedBorder(Color.blue));
        add(panel);

        model = new DefaultTableModel();
        table = new JTable(model);
        table.setEnabled(false);

        model.addColumn("ID");
        model.addColumn("Name");
        model.addColumn("Gender");
        model.addColumn("Address");
        model.addColumn("Contact");

        scrollpane = new JScrollPane(table,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        panel.add(scrollpane);
        setResizable(false);
        setVisible(true);
    }

    public static void main(String[] args) {
        new Form();
    }
}