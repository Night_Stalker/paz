package org.example;

import javakara.JavaKaraProgram;

/*
 * COMMANDS:
 *   kara.move()           kara.turnRight()      kara.turnLeft()
 *   kara.putLeaf()        kara.removeLeaf()
 * SENSORS:
 *   kara.treeFront()      kara.treeLeft()       kara.treeRight()
 *   kara.mushroomFront()  kara.onLeaf()
 */
public class Lab4_3 extends JavaKaraProgram {
    //
    // you can define your methods here:
    //
    public void myProgram() {
        // put your main program here, for example:
        boolean inTunnel = false;
        while (!kara.treeFront()) {
            if (!inTunnel && kara.treeRight() && kara.treeLeft()) {
                inTunnel = true;
            }
            if (inTunnel && (!kara.treeRight() || !kara.treeLeft())) {
                break;

            }
            kara.move();
        }
    }
}

        