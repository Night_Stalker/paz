package org.example;

import javakara.JavaKaraProgram;

import java.awt.*;

public class GameOfLife extends JavaKaraProgram {

    void leftUpKaraMoves() {
        kara.turnLeft();
        kara.move();
        kara.turnLeft();
        kara.move();
    }

    boolean leftUp() {
        leftUpKaraMoves();
        boolean returnValue = kara.onLeaf();
        leftUpKaraMoves();
        return returnValue;
    }

    void leftKaraMoves() {
        kara.turnLeft();
        kara.turnLeft();
        kara.move();
    }

    boolean left() {
        leftKaraMoves();
        boolean returnValue = kara.onLeaf();
        leftKaraMoves();
        return returnValue;
    }

    void leftDownKaraMoves() {
        kara.turnRight();
        kara.move();
        kara.turnRight();
        kara.move();
    }

    boolean leftDown() {
        leftDownKaraMoves();
        boolean returnValue = kara.onLeaf();
        leftDownKaraMoves();
        return returnValue;
    }

    void upKaraMoves() {
        kara.turnLeft();
        kara.move();
    }

    boolean up() {
        upKaraMoves();
        boolean returnValue = kara.onLeaf();
        kara.turnLeft();
        upKaraMoves();
        kara.turnLeft();
        return returnValue;
    }

    void downKaraMoves() {
        kara.turnRight();
        kara.move();
    }

    boolean down() {
        downKaraMoves();
        boolean returnValue = kara.onLeaf();
        kara.turnRight();
        downKaraMoves();
        kara.turnRight();
        return returnValue;
    }

    void rightUpKaraMoves() {
        kara.move();
        kara.turnLeft();
        kara.move();
    }

    boolean rightUp() {
        rightUpKaraMoves();
        boolean returnValue = kara.onLeaf();
        kara.turnLeft();
        rightUpKaraMoves();
        kara.turnLeft();
        return returnValue;
    }

    void rightKaraMoves() {
        kara.move();
        kara.turnLeft();
        kara.turnLeft();
    }

    boolean right() {
        rightKaraMoves();
        boolean returnValue = kara.onLeaf();
        rightKaraMoves();
        return returnValue;
    }

    void rightDownKaraMoves() {
        kara.move();
        kara.turnRight();
        kara.move();
        kara.turnRight();
    }

    boolean rightDown() {
        rightDownKaraMoves();
        boolean returnValue = kara.onLeaf();
        rightDownKaraMoves();
        return returnValue;
    }

    void configuration() {
        kara.setPosition(0, 0);
    }

    int karaCounting(boolean[][] checkingPoints) {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (checkingPoints[i][j]) {
                    switch (i) {
                        case 0 -> {
                            switch (j) {
                                case 0 -> {
                                    if (leftUp()) {
                                        count++;
                                    }
                                }
                                case 1 -> {
                                    if (left()) {
                                        count++;
                                    }
                                }
                                case 2 -> {
                                    if (leftDown()) {
                                        count++;
                                    }
                                }
                            }
                        }
                        case 1 -> {
                            switch (j) {
                                case 0 -> {
                                    if (up()) {
                                        count++;
                                    }
                                }
                                case 2 -> {
                                    if (down()) {
                                        count++;
                                    }
                                }
                            }
                        }
                        case 2 -> {
                            switch (j) {
                                case 0 -> {
                                    if (rightUp()) {
                                        count++;
                                    }
                                }
                                case 1 -> {
                                    if (right()) {
                                        count++;
                                    }
                                }
                                case 2 -> {
                                    if (rightDown()) {
                                        count++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
//        if (count != 0) {
//            tools.showMessage(String.valueOf(count));
//        }
        return count;
    }

    void checkKaraMove(boolean[][] checkingPoints, int x, int y, int[][] worldSize) {
        Point karaPosition = kara.getPosition();
//        tools.showMessage(String.valueOf(karaPosition.getY() + x - 1) + ":" + (karaPosition.getX() + y - 1) + "\n" +
//                karaPosition.getX() + ":" + (karaPosition.getY()));
        if (worldSize[(int) karaPosition.getX() + x - 1][(int) karaPosition.getY() + y - 1] != 1) {
            checkingPoints[x][y] = true;
        }
    }

    void checkCondition(int[][] worldSize, int sizeX, int sizeY) {
        Point karaPosition = kara.getPosition();
        int karaX = (int) karaPosition.getX();
        int karaY = (int) karaPosition.getY();

        boolean[][] checkingPoints = new boolean[3][3];
        if (karaX == 0 && karaY == 0) {
            // zuun deed
            checkKaraMove(checkingPoints, 1, 2, worldSize);
            checkKaraMove(checkingPoints, 2, 1, worldSize);
            checkKaraMove(checkingPoints, 2, 2, worldSize);
        } else if (karaX == 0 && karaY == sizeY) {
            // zuun dood
            checkKaraMove(checkingPoints, 1, 0, worldSize);
            checkKaraMove(checkingPoints, 2, 0, worldSize);
            checkKaraMove(checkingPoints, 2, 1, worldSize);
        } else if (karaX == sizeX && karaY == 0) {
            // baruun deed
            checkKaraMove(checkingPoints, 0, 1, worldSize);
            checkKaraMove(checkingPoints, 0, 2, worldSize);
            checkKaraMove(checkingPoints, 1, 2, worldSize);
        } else if (karaX == sizeX && karaY == sizeY) {
            // baruun dood
            checkKaraMove(checkingPoints, 0, 0, worldSize);
            checkKaraMove(checkingPoints, 0, 1, worldSize);
            checkKaraMove(checkingPoints, 1, 0, worldSize);
        } else if (karaY == 0) {
            // deed
            checkKaraMove(checkingPoints, 0, 1, worldSize);
            checkKaraMove(checkingPoints, 0, 2, worldSize);
            checkKaraMove(checkingPoints, 1, 2, worldSize);
            checkKaraMove(checkingPoints, 2, 1, worldSize);
            checkKaraMove(checkingPoints, 2, 2, worldSize);
        } else if (karaX == 0) {
            // zuun
            checkKaraMove(checkingPoints, 1, 0, worldSize);
            checkKaraMove(checkingPoints, 1, 2, worldSize);
            checkKaraMove(checkingPoints, 2, 0, worldSize);
            checkKaraMove(checkingPoints, 2, 1, worldSize);
            checkKaraMove(checkingPoints, 2, 2, worldSize);
        } else if (karaY == sizeY) {
            // dood
            checkKaraMove(checkingPoints, 0, 0, worldSize);
            checkKaraMove(checkingPoints, 0, 1, worldSize);
            checkKaraMove(checkingPoints, 1, 0, worldSize);
            checkKaraMove(checkingPoints, 2, 0, worldSize);
            checkKaraMove(checkingPoints, 2, 1, worldSize);
        } else if (karaX == sizeX) {
            // baruun
            checkKaraMove(checkingPoints, 0, 0, worldSize);
            checkKaraMove(checkingPoints, 0, 1, worldSize);
            checkKaraMove(checkingPoints, 0, 2, worldSize);
            checkKaraMove(checkingPoints, 1, 0, worldSize);
            checkKaraMove(checkingPoints, 1, 2, worldSize);
        } else {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (worldSize[karaX + i - 1][karaY + j - 1] != 1) {
                        checkingPoints[i][j] = true;
                    }
                }
            }
        }

//        for (int i = 0; i < 3; i++) {
//            for (int j = 0; j < 3; j++) {
//                if (!checkingPoints[i][j]) {
//                    tools.showMessage(i + ":" + j + "=" + checkingPoints[i][j]);
//                }
//            }
//        }

        int countOfLeaf = karaCounting(checkingPoints);
        if (countOfLeaf < 2 || countOfLeaf > 3) {
            if (kara.onLeaf()) {
                kara.removeLeaf();
                worldSize[karaX][karaY] = 1;
//                tools.showMessage(karaX + ":" + karaY);
            }
        } else if (countOfLeaf == 3 && !kara.onLeaf()) {
            kara.putLeaf();
            worldSize[karaX][karaY] = 1;
//            tools.showMessage(karaX + ":" + karaY);
        }
    }

    public void myProgram() {
        configuration();
        int sizeX = world.getSizeX();
        int sizeY = world.getSizeY();
        int[][] worldSize = new int[sizeX][sizeY];
        while (true) {
            checkCondition(worldSize, sizeX - 1, sizeY - 1);
            if ((int) kara.getPosition().getX() == sizeX - 1) {
                if ((int) kara.getPosition().getX() == sizeX - 1 && (int) kara.getPosition().getY() == sizeY - 1) {
                    for (int i = 0; i < worldSize.length; i++) {
                        for (int j = 0; j < worldSize[i].length; j++) {
                            worldSize[i][j] = 0;
                        }
                    }
//                    tools.showMessage("cleared");
                }
                kara.move();
                kara.turnRight();
                kara.move();
                kara.turnLeft();
            } else {
                kara.move();
            }
        }
    }
}
