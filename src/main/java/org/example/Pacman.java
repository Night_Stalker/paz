package org.example;

import javakara.JavaKaraProgram;

public class Pacman extends JavaKaraProgram {

    private void findLeaf() {
        do {
            kara.turnLeft();
            kara.turnLeft();
            kara.move();
            kara.turnRight();
            kara.move();
        } while (!kara.onLeaf());
    }

    private void takeLeaf() {
        if (kara.onLeaf()) {
            kara.removeLeaf();
            if (!kara.treeFront()) {
                kara.move();
            }
        }
    }

    public void myProgram() {
        while (!kara.treeFront()) {
            takeLeaf();
            if (!kara.onLeaf()) {
                findLeaf();
            }

        }
        takeLeaf();
    }
}