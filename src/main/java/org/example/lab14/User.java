package org.example.lab14;

public class User {
    Integer id;
    String name;
    String gender;
    String address;
    String contact;

    User(Integer id,
         String name,
         String gender,
         String address,
         String contact) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.address = address;
        this.contact = contact;
    }
}
