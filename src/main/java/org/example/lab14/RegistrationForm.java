package org.example.lab14;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;


public class RegistrationForm extends JFrame implements ActionListener {

    JLabel title;
    JLabel idLabel;
    JLabel nameLabel;
    JLabel genderLabel;
    JLabel addressLabel;
    JLabel contactLabel;
    JTextField idField;
    JTextField nameField;
    JTextField addressField;
    JTextField contactField;
    JButton registerButton;
    JButton exitButton;
    JRadioButton male;
    JRadioButton female;
    ButtonGroup bg;
    JPanel panel;
    JTable table;
    DefaultTableModel model;
    JScrollPane scrollPanel;
    String gender;
    private final List<User> userList = new ArrayList<>();

    RegistrationForm() {
        setSize(700, 370);
        setLayout(null);

        title = new JLabel("Registration RegistrationForm");
        title.setBounds(60, 7, 200, 30);
        idLabel = new JLabel("ID");
        idLabel.setBounds(30, 50, 60, 30);
        nameLabel = new JLabel("Name");
        nameLabel.setBounds(30, 90, 60, 30);
        genderLabel = new JLabel("Gender");
        genderLabel.setBounds(30, 130, 60, 30);
        addressLabel = new JLabel("Address");
        addressLabel.setBounds(30, 170, 60, 30);
        contactLabel = new JLabel("Contact");
        contactLabel.setBounds(30, 210, 60, 30);

        idField = new JTextField();
        idField.setBounds(120, 50, 100, 30);
        idField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
                    e.consume();
                }
            }
        });

        nameField = new JTextField();
        nameField.setBounds(120, 90, 100, 30);
        addressField = new JTextField();
        addressField.setBounds(120, 170, 100, 30);
        contactField = new JTextField();
        contactField.setBounds(120, 210, 100, 30);

        male = new JRadioButton("Male");
        male.setBounds(95, 130, 60, 30);
        male.addActionListener(e -> gender = "Male");

        female = new JRadioButton("Female");
        female.setBounds(155, 130, 70, 30);
        female.addActionListener(e -> gender = "Female");

        bg = new ButtonGroup();
        bg.add(male);
        bg.add(female);

        exitButton = new JButton("Exit");
        exitButton.setBounds(30, 250, 85, 30);
        exitButton.addActionListener(this);

        registerButton = new JButton("Register");
        registerButton.setBounds(120, 250, 100, 30);
        registerButton.addActionListener(this);

        add(title);

        add(idLabel);
        add(nameLabel);
        add(genderLabel);
        add(addressLabel);
        add(contactLabel);

        add(idField);
        add(nameField);
        add(addressField);
        add(contactField);
        add(male);
        add(female);

        add(exitButton);
        add(registerButton);

        panel = new JPanel();
        panel.setLayout(new GridLayout());
        panel.setBounds(250, 10, 400, 300);
        panel.setBorder(BorderFactory.createDashedBorder(Color.blue));
        add(panel);

        model = new DefaultTableModel();
        table = new JTable(model);
        table.setEnabled(false);

        model.addColumn("ID");
        model.addColumn("Name");
        model.addColumn("Gender");
        model.addColumn("Address");
        model.addColumn("Contact");

        scrollPanel = new JScrollPane(table,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        panel.add(scrollPanel);
        setResizable(false);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == exitButton) {
            System.exit(0);
        }

        if (ae.getSource() == registerButton) {
            if (idField.getText().isEmpty() ||
                    nameField.getText().isEmpty() ||
                    gender.isEmpty() ||
                    addressField.getText().isEmpty() ||
                    contactField.getText().isEmpty())

                JOptionPane.showMessageDialog(idField, "Fields will not be blank");
            else {
                int id = Integer.parseInt(idField.getText());
                if (!isUserInList(id)) {
                    userList.add(new User(id, nameField.getText(), gender, addressField.getText(), contactField.getText()));
                    addRows();
                    JOptionPane.showMessageDialog(this, "Successfully Registered");
                    idField.setText("");
                    nameField.setText("");
                    gender = "";
                    contactField.setText("");
                    addressField.setText("");
                    bg.clearSelection();
                } else {
                    JOptionPane.showMessageDialog(idField, "Id is exists");
                }
            }
        }
    }

    private boolean isUserInList(int newId) {
        for (User user : this.userList) {
            if (user.id == newId) {
                return true;
            }
        }
        return false;
    }

    public void addRows() {
        User lastUser = userList.get(userList.size() - 1);
        String string = lastUser.id + "," + lastUser.name + "," + lastUser.gender + "," +
                lastUser.address + "," + lastUser.contact;
        Object[] row = string.split(",");
        model.addRow(row);
        panel.revalidate();
    }

    public static void main(String[] args) {
        new RegistrationForm();
    }
}