package org.example.lab15;

class Car {
    int year;
    double engine;
    String model;
    Car next;

    Car() {
        year = 2022;
        engine = 2.0;
        model = "Kia";
    }

    Car(int y, double e, String m) {
        year = y;
        engine = e;
        model = m;
    }
}

public class CarList {
    int total;
    Car head;
    Car back;

    CarList() {
        head = null;
        total = 0;
    }

    public void SortYear() {
        Car newcar = new Car();
        back = head;
        // sortoo hiih code-g bichne uu

    }

    public void addFront(Car c) {
        Car newcar = new Car();
        newcar = c;
        newcar.next = head;
        head = newcar;
        total++;
    }

    public void addBack(Car c) {
        back = head;
        while (back.next != null) {
            back = back.next;
        }
        Car newcar = new Car();
        newcar = c;
        back.next = newcar;
        total++;
    }

    public void addMid(Car c, int p) {
        Car newcar = new Car();
        newcar = c;
        int i = 0;
        back = head;
        while (i < p - 1) {
            back = back.next;
            i++;
        }
        newcar.next = back.next;
        back.next = newcar;
        total++;
    }


    public void show() {
        back = head;
        while (back != null) {
            System.out.print("\n\nYear: " + back.year + "\nengine: " + back.engine + "\nmodel: " + back.model);
            back = back.next;
        }
    }

    public void showNumberOfCars() {
        System.out.println("\n\nNumber of cars: " + total);
    }

    public void delLast() {
        back = head;
        while (back.next.next != null) {
            back = back.next;
        }
        back.next = null;
        total--;
    }

    public void delFirst() {
        head = head.next;
        total--;
    }

    public void delMiddle(int position) {
        back = head;
        int i = 0;
        while (i < position - 1) {
            back = back.next;
            i++;
        }
        back.next = back.next.next;
        total--;
    }

    public static void main(String[] args) {

        CarList carList = new CarList();

        Car c1 = new Car(2023, 3, "Hyundai");
        Car c2 = new Car(2022, 4, "Toyota");
        Car c3 = new Car(2023, 3, "Mazda");
        Car c4 = new Car(2022, 4, "FORD");
        Car c5 = new Car(2000, 1, "poni");


        carList.addFront(c5);
        carList.addFront(c4);
        carList.addFront(c3);

        carList.addMid(c1, 2);

        carList.addBack(c2);

//        carList.delFirst();
//        carList.delLast();
        carList.delMiddle(2);

        carList.show();
        carList.showNumberOfCars();
    }
}

